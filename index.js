console.log("Hello Amigos");

let trainer = {
	name: "Superman loloko",
	age: 69,
	pokemon: ["Bulbasaur", "Captain Barbel", "Asumon", "Blue Eyes Black Dolphin"],
	friends: {
		tondo: ["Kuya Kim", "Ash Ketchup"],
		czechoslovakia: ["Zienab Clee", "Brock Lesnar"]
	},
	talk: function(){
		console.log("Blue Eyes Black Dolphin I choose you!")
	}
}

console.log(trainer);

console.log("Result of Dot Notation:");
console.log(trainer.name);

console.log("Result of Dot Notation:");
console.log(trainer["pokemon"]);

console.log("Result of Talk Method:");
trainer.talk();


function Pokemon(name, level){

	this.name = name;
	this.level = level;
	this.health = 69 * level;
	this.attack = 32 * level;

	this.tackle = function(target) {

        console.log(this.name + ' tackled ' + target.name);
        target.health -= this.attack;

       	if (target.health <= 0){
       		console.log(target.name + " 's health is now reduce to " + target.health)
       		console.log(this.name + ' fainted');
        } else {
        	console.log(target.name + " 's health is now reduce to " + target.health)
        };

    };

    this.faint = function(){
        console.log(this.name + ' fainted')
    }

};

let bulbasaur = new Pokemon("Bulbasaur", 305);
let captainBarbel = new Pokemon("Captain Barbel", 206);
let asumon = new Pokemon("Asumon", 135);
let blueEyesBlackDolphin = new Pokemon("Blue Eyes Black Dolphin", 69);

console.log(bulbasaur);
console.log(captainBarbel);
console.log(asumon);
console.log(blueEyesBlackDolphin);

blueEyesBlackDolphin.tackle(captainBarbel);
console.log(captainBarbel);

bulbasaur.tackle(asumon);
console.log(asumon);

